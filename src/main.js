import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '@/components/Login.vue'
import Home from '@/components/Home.vue'
import Signup from '@/components/Signup'
import ForgotPassword from '@/components/ForgotPassword'
import Home2 from '@/components/Home2'
import Home3 from '@/components/Home3'
import Home4 from '@/components/Home4'
import Home5 from '@/components/Home5'
import Home6 from '@/components/Home6'
import Home7 from '@/components/Home7'
import Home8 from '@/components/Home8'
import Home9 from '@/components/Home9'

import { library } from '@fortawesome/fontawesome-svg-core'
import { 
    faFile, faTasks, faList, faAngleLeft, faSignOutAlt, faBars, faCircle, 
    faArrowCircleRight, faTimes, faQuestionCircle, faInfoCircle, faUserCircle, 
    faBook, faMapMarkerAlt, faPencilAlt,  faPlus, faSpinner,faQuoteLeft, faQuestion,
    faSyncAlt, faThumbsUp,faThumbsDown, faSignInAlt, faMinus
    } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faHackerrank, faGoogle, faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons'
import {faBell, faFileAlt, faCheckCircle} from '@fortawesome/free-regular-svg-icons'

library.add(faFile, faHackerrank, faTasks, faList, faAngleLeft, faBell, 
    faSignOutAlt, faBars, faCircle, faGoogle, faArrowCircleRight, faTimes, 
    faQuestionCircle, faInfoCircle, faUserCircle, faBook, faMapMarkerAlt, 
    faPencilAlt, faFileAlt, faPlus, faSpinner, faQuoteLeft, faQuestion,
    faSyncAlt,faThumbsUp,faThumbsDown, faCheckCircle, faSignInAlt, faFacebook, faTwitter, faInstagram, faMinus
    )
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = {
    '/': Home,
    '/login': Login,
    '/register': Signup,
    '/forgot-password': ForgotPassword,
    '/home-2': Home2,
    '/home-3': Home3,
    '/home-4': Home4,
    '/home-5': Home5,
    '/home-6': Home6,
    '/home-7': Home7,
    '/home-8': Home8,
    '/home-9': Home9
}

new Vue({
    data: {
        currentRoute: window.location.pathname,
        currentTheme: 'red',
        isParticles: false
    },
    computed: {
        ViewComponent() {
            if (this.currentRoute == '/home-9') {
                // eslint-disable-next-line vue/no-side-effects-in-computed-properties
                this.isParticles = true;
            }
            return routes[this.currentRoute]
        }
    },
    mounted: function () {
        // Navbar
        function initNavbar() {
            // eslint-disable-next-line no-undef
            jQuery('.navbar-nav a').bind('click', function (event) {
                // eslint-disable-next-line no-undef
                var jQueryanchor = jQuery(this);
                // eslint-disable-next-line no-undef
                jQuery('html, body').stop().animate({
                    // eslint-disable-next-line no-undef
                    scrollTop: jQuery(jQueryanchor.attr('href')).offset().top - 0
                }, 1500, 'easeInOutExpo');
                event.preventDefault();
            });
        }

        // Sticky Header
        function initSticky() {
            // eslint-disable-next-line no-undef        
            jQuery(".sticky").sticky({
                topSpacing: 0
            });
        }

        // Magnific Popup
        function initMagnificPopup() {
            // eslint-disable-next-line no-undef
            jQuery('.video-play-icon').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,

                fixedContentPos: false
            });
        }

        function initContactForm() {
            // eslint-disable-next-line no-undef
            jQuery('#contact-form').submit(function () {

                // eslint-disable-next-line no-undef
                var action = jQuery(this).attr('action');

                // eslint-disable-next-line no-undef
                jQuery("#message").slideUp(750, function () {
                    // eslint-disable-next-line no-undef
                    jQuery('#message').hide();
                    // eslint-disable-next-line no-undef
                    jQuery('#submit')
                        .attr('disabled', 'disabled');
                    // eslint-disable-next-line no-undef
                    jQuery.post(action, {
                        // eslint-disable-next-line no-undef
                        name: jQuery('#name').val(),
                        // eslint-disable-next-line no-undef
                        email: jQuery('#email').val(),
                        // eslint-disable-next-line no-undef
                        comments: jQuery('#comments').val(),
                    },
                        function (data) {
                            document.getElementById('message').innerHTML = data;
                            // eslint-disable-next-line no-undef
                            jQuery('#message').slideDown('slow');
                            // eslint-disable-next-line no-undef
                            jQuery('#cform img.contact-loader').fadeOut('slow', function () {
                                // eslint-disable-next-line no-undef
                                jQuery(this).remove()
                            });
                            // eslint-disable-next-line no-undef
                            jQuery('#submit').removeAttr('disabled');
                            // eslint-disable-next-line no-undef
                            if (data.match('success') != null) jQuery('#cform').slideUp('slow');
                        }
                    );

                });

                return false;
            });
        }

        function init() {
            initNavbar();
            initSticky();
            initMagnificPopup();
            initContactForm();
            // eslint-disable-next-line no-undef
            Waves.init();
        }
        init();
        if (this.isParticles) {
            //load particles
            // eslint-disable-next-line no-undef
            jQuery('head').append("<!--Partical js--><script src='./js/particles.js'></script><script src='./js/particles.app.js'></script>");
        }
        // eslint-disable-next-line no-undef
        jQuery("#style-switcher .bottom a.settings").click(function (e) {
            e.preventDefault();
            // eslint-disable-next-line no-undef
            var div = jQuery("#style-switcher");
            if (div.css("left") === "-189px") {
                // eslint-disable-next-line no-undef
                jQuery("#style-switcher").animate({
                    left: "0px"
                });
            } else {
                // eslint-disable-next-line no-undef
                jQuery("#style-switcher").animate({
                    left: "-189px"
                });
            }
        })

        // eslint-disable-next-line no-undef
        jQuery("ul.pattern li a").click(function (e) {
            e.preventDefault();
            // eslint-disable-next-line no-undef
            jQuery(this).parent().parent().find("a").removeClass("active");
            // eslint-disable-next-line no-undef
            jQuery(this).addClass("active");
        })

        // eslint-disable-next-line no-undef
        jQuery("#style-switcher").animate({
            left: "-189px"
        });
    },
    methods: {
        handleScroll() {
            // eslint-disable-next-line no-undef
            var scrollDistance = jQuery(window).scrollTop();
            // Assign active class to nav links while scolling
            // eslint-disable-next-line no-undef
            jQuery('section.scroll-select').each(function (i) {
                // eslint-disable-next-line no-undef
                if (jQuery(this).position().top <= scrollDistance + 160) {
                    // eslint-disable-next-line no-undef
                    jQuery('#mySidenav li.active').removeClass('active');
                    // eslint-disable-next-line no-undef
                    jQuery('#mySidenav li').eq(i).addClass('active');
                }
            });
        }
    },
    created() {
        window.addEventListener('scroll', this.handleScroll);
    },
    destroyed() {
        window.removeEventListener('scroll', this.handleScroll);
    },
    render(h) { return h(this.ViewComponent) }
}).$mount('#dorsionApp')
